<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>PPAY</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript">
	function processNewTransaction()
	{
		processEncryptedCreditCardInformation();
		return;
		var dataArray = {
			"creditCardNumber" : $("#creditCardNumber").val(),
			"cvv" : $("#cvv").val(),
			"expirationDate" : $("#expirationDate").val(),
		    "amount" : $("#amount").val(),
		    firstName : $("#firstName").val(),
        	lastName : $("#lastName").val(),
        	telePhone : $("#telePhone").val(),
        	street : $("#street").val(),
        	street2 : $("#street2").val(),
        	city : $("#city").val(),
        	state : $("#state").val(),
        	country : $("#country").val(),
        	postalCode: $("#postalCode").val(), 
        	company: $("#company").val(), 
        	emailAddress : $("#emailAddress").val(),
        	comments : $("#comments").val()
		};

		 $.ajax({
			 url: "<%=request.getContextPath().toString()%>/creditcardpayment",
		 	 dataType : "json",
		 	 data : dataArray,
		 	 method: "get",
		 	 global : false,
			 success: function(response){
				 if(response.success)
			     {
					 $("#message-div").html('<div class="alert alert-success" role="alert">Payment completed successfully.&nbsp&nbspTransaction Id: '+response.message+'</div>');
			     }
				 else
				{
					 $("#message-div").html('<div class="alert alert-danger" role="alert">Please enter valid credit card information</div>');
				}
		 	 },
		 	 error: function(result){
		 		 $("#message-div").html('<div class="alert alert-danger" role="alert">Please enter valid credit card information</div>');
		 	 }
		});
	}
	
	function processEncryptedCreditCardInformation()
	{
		var encDataBase64 = 'bjCZmEXbz5KCzE0OOQs7Bnas7MVTBDBl3NJ-Ol6fcdOJyQLbm-8neDnA5lgVt_xFg3EfjpFBMGBt8mMdAJxB433rJrN0d8VJPSLsvukZc1JpfQzNmWKRiV29AzFyWhl3wsNremw805A-uWCmDxsMcSCHZIT_ANBK0JPn7M_FmT8DrszMegNiQFweafgxoEQ-nsZhuiEmVfLX5B7xI9TG3n-ByG4DD6AEWibK3Rgy7ts5dJPg5ZE37ez5myR2amVyqLwWjL9YESLpEGYjz5sTr_GPaxG_JgogcZaTj8UchBSTwwtgoq0IGKy0EpsyqDG_dapB-hSGA47MWwjjl6ZPLxZfxnJVl1beLMBqVjdLEYUVNiSzUvLmymg_gDLAxVfqepvBXKmbEBzcqMp3EG_n-cAb6p8fcOvCwiB99c3G1BPZNaPik3rs4VnL7CqfprDsc5HuVV6C7ydiwkaA3geaGT7Xi3L8oAmL3C3GO-mRp9ah9ZHECJPdnCWiN9FWmeFA';
		 $.ajax({
			 url: "<%=request.getContextPath().toString()%>/v1/paymentgateway/"+encDataBase64,
		 	 dataType : "json",
		 	 method: "get",
		 	 global : false,
			 success: function(response){
				 if(response.success)
			     {
					 $("#message-div").html('<div class="alert alert-success" role="alert">Payment completed successfully.&nbsp&nbspTransaction Id: '+response.message+'</div>');
			     }
				 else
				{
					 $("#message-div").html('<div class="alert alert-danger" role="alert">Please enter valid credit card information</div>');
				}
		 	 },
		 	 error: function(result){
		 		 $("#message-div").html('<div class="alert alert-danger" role="alert">Please enter valid credit card information</div>');
		 	 }
		});	
	}

</script>
<style>
.indication {
  color: red;
}
</style>
</head>
<body>
	<div class="container">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h2>PPAY</h2>
			</div>
			<div class="panel-body-pabel-primary"></div>
			<div class="panel-body panel-primary">
			<div class="row">
				<div class="col-sm-6 col-sm-offset-3" >
					<h3>Please enter credit card information</h3>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6 col-sm-offset-3" id="message-div">
				</div>
			</div>
				<form
					action="<%=request.getContextPath().toString()%>/creditcardpayment"
					method="post">
					<div class="form-group row">
						<label for="creditCardNumber" class="col-sm-3 col-form-label">Credit
							Card Number<span class="indication">*</span>:</label>
						<div class="col-sm-6">
							<input type="text" class="form-control"
								id="creditCardNumber" name="creditCardNumber">
						</div>
					</div>

					<div class="form-group row">
						<label for="creditCardNumber" class="col-sm-3 col-form-label">CVV<span class="indication">*</span>:</label>
						<div class="col-sm-6">
							<input type="text" class="form-control"
								id="cvv" name="cvv">
						</div>
					</div>
					<div class="form-group row">
						<label for="creditCardNumber" class="col-sm-3 col-form-label">Expiration Date<span class="indication">*</span>:</label>
						<div class="col-sm-6">
							<input type="text" class="form-control"
								id="expirationDate" name="expirationDate">
						</div>
					</div>
					<div class="form-group row">
						<label for="creditCardNumber" class="col-sm-3 col-form-label">Amount<span class="indication">*</span>:</label>
						<div class="col-sm-6">
							<input type="text" class="form-control"
								id="amount" name="amount">
						</div>
					</div>
					<div class="form-group row">
						<label for="firstName" class="col-sm-3 col-form-label">First Name<span class="indication">*</span>:</label>
						<div class="col-sm-6">
							<input type="text" class="form-control"
								id="firstName" name="firstName">
						</div>
					</div>
					<div class="form-group row">
						<label for="lastName" class="col-sm-3 col-form-label">Last Name<span class="indication">*</span>:</label>
						<div class="col-sm-6">
							<input type="text" class="form-control"
								id="lastName" name="lastName">
						</div>
					</div>
					<div class="form-group row">
						<label for="street" class="col-sm-3 col-form-label">Street<span class="indication">*</span>:</label>
						<div class="col-sm-6">
							<input type="text" class="form-control"
								id="street" name="street">
						</div>
					</div>
					<div class="form-group row">
						<label for="street2" class="col-sm-3 col-form-label">Street2:</label>
						<div class="col-sm-6">
							<input type="text" class="form-control"
								id="street2" name="street2">
						</div>
					</div>
					<div class="form-group row">
						<label for="city" class="col-sm-3 col-form-label">City<span class="indication">*</span>:</label>
						<div class="col-sm-6">
							<input type="text" class="form-control"
								id="city" name="city">
						</div>
					</div>
					<div class="form-group row">
						<label for="state" class="col-sm-3 col-form-label">State/Region<span class="indication">*</span>:</label>
						<div class="col-sm-6">
							<input type="text" class="form-control"
								id="state" name="state">
						</div>
					</div>
					<div class="form-group row">
						<label for="country" class="col-sm-3 col-form-label">Country<span class="indication">*</span>:</label>
						<div class="col-sm-6">
						<input type="hidden" id="country" name="country" value="usa">
							United States Only
						</div>
					</div>
					<div class="form-group row">
						<label for="postalCode" class="col-sm-3 col-form-label">Postal Code<span class="indication">*</span>:</label>
						<div class="col-sm-6">
							<input type="text" class="form-control"
								id="postalCode" name="postalCode">
						</div>
					</div>
					<div class="form-group row">
						<label for="emailAddress" class="col-sm-3 col-form-label">Email Address<span class="indication">*</span>:</label>
						<div class="col-sm-6">
							<input type="text" class="form-control"
								id="emailAddress" name="emailAddress">
						</div>
					</div>
					<div class="form-group row">
						<label for="state" class="col-sm-3 col-form-label">Telephone<span class="indication">*</span>:</label>
						<div class="col-sm-6">
							<input type="text" class="form-control"
								id="telePhone" name="telePhone">
						</div>
					</div>
					<div class="form-group row">
						<label for="company" class="col-sm-3 col-form-label">Company:</label>
						<div class="col-sm-6">
							<input type="text" class="form-control"
								id="company" name="company">
						</div>
					</div>
					<div class="form-group row">
						<div class="col-sm-6 col-sm-offset-3">
							 <span class="indication">*</span> Indicates Required Fields
						</div>
					</div>
					<div class="form-group row">
						<label for="state" class="col-sm-3 col-form-label">Comments: (ex. Special shipping instructions</label>
						<div class="col-sm-6">
							<input type="text" class="form-control"
								id="comments" name="comments">
						</div>
					</div>
					<div class="form-group row">
						<div class="col-sm-6 col-sm-offset-3">
							<input type="button" class="btn btn-primary" value="Submit" onclick="processNewTransaction()">
						</div>
					</div>
				</form>
			</div>	 
		</div>
	</div>
</body>
</html>