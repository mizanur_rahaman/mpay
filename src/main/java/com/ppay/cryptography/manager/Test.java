package com.ppay.cryptography.manager;

import java.io.IOException;
import java.security.Key;
import java.security.spec.AlgorithmParameterSpec;
import java.util.Base64;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.ppay.dto.CreditCardPaymentModel;
import com.ppay.dto.PaymentInformationModel;
import com.ppay.dto.ANetApiKeyModel;
import com.ppay.dto.CreditCardModel;

public class Test {
	String key = "dAtAbAsE98765432";
	public String encryptData(String data)
	{

		Key aesKey = new SecretKeySpec(key.getBytes(), "AES");
		try 
		{
			Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
			cipher.init(Cipher.ENCRYPT_MODE, aesKey);
			byte[] encrypted = cipher.doFinal(data.getBytes());
			System.err.println("Encrypted: " + new String(Base64.getUrlEncoder().encodeToString(encrypted)));
			return new String(Base64.getUrlEncoder().encodeToString(encrypted));
			//this.dycryptData(encrypted);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return null;
	}
	
	public String dycryptData(byte[] encrypted)
	{

		Key aesKey = new SecretKeySpec(key.getBytes(), "AES");
		try {
			Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
			cipher.init(Cipher.DECRYPT_MODE, aesKey);
			String decrypted = new String(cipher.doFinal(encrypted));
			return decrypted;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	public static void main (String args[])
	{
		//creditCardInformation = "creditCardNumber=442222&cvv=123&expirationDate=0820&amount=100&firstName=Ricardo&lastName=Kaka&telePhone=334343&street=32%2C2%20B%20Playmouth&street2=&city=PlayMouth&state=New%20Yourk&country=usa&postalCode=34343&company=placovu&emailAddress=kaka%40gmail.com&comments=sent%20to%20the%20new%20york";
				//byte[] encryptedData =  cryptographyManager.encryptData(encCreditCardInformation);
				//System.out.println("encrypted :" +  encryptedData);
				
				//encCreditCardInformation = "bjCZmEXbz5KCzE0OOQs7Bnas7MVTBDBl3NJ+Ol6fcdOJyQLbm+8neDnA5lgVt/xFz20U/Rd/kQbfazflJO++9P0orQPjuRvV0PRH3LCoUYwsenZeXx162pGp5AXI6bVDftQQERvMOpgB/HauI2vKw4dA+QF8bwpPZDnZPQnoryI4ZLsjrmkj/agJRXYSQpxGjgzUb0bDPKmlsenGNYzurWhQgt3KTaj1yE3NfQZi7pjqyaUk60qFwjZICVVwTxdNC3oYNUNLhwlzIiv+Xtvnl1ducwkRPcx538kyRoEjy4+nRGBcGb4VcSxsdPMMT1VnP9EUXN6D+ya0w9QR1cbMBS1iK0bCVnwXexOvhTpiDtPG+eCbZQtOorcVOmxKxJ/lEW4qFZZX7pQAjh2G4THpGMCTCL7AxHPUNrLg57HTiLDlxZGv4GJAB9AiB0xUZYfp1wbBtP0PI+/ZqyClRwr1rL0bbldwSa1Z7Q9JyoXDSBw=";
				//encCreditCardInformation = "P2s1CjH40dwoPREOkDa-5Q==";
		Test cm = new Test();
		//String creditCardInformation = "apiLoginId=6y84QxNqEHUm&transactionKey=9pF9CxH2a3g5q352&cardNumber=4242424242424242&cardCode=123&expirationDate=0820&amount=100&firstName=Ricardo&lastName=Kaka&telePhone=334343&street=32%2C2%20B%20Playmouth&street2=&city=PlayMouth&state=New%20Yourk&country=usa&postalCode=34343&company=placovu&emailAddress=kaka%40gmail.com&comments=sent%20to%20the%20new%20york&refId=1000";
         
		
		
		CreditCardPaymentModel creditCardModel = new CreditCardPaymentModel();
		ANetApiKeyModel apm = new ANetApiKeyModel();
		apm.setApiLoginId("6y84QxNqEHUm");
		apm.setApiTransactionKey("9pF9CxH2a3g5q352");
		creditCardModel.setaNetApiKeyViewModel(apm);
		CreditCardModel creditCardViewModel = new CreditCardModel();
		creditCardViewModel.setCreditCardNumber("4222222222222");
		creditCardViewModel.setCardCode("123");
		creditCardViewModel.setExpirationDate("08/20");
		PaymentInformationModel pp = new PaymentInformationModel();
		pp.setOrderTotal(20);
		creditCardModel.setCreditCardViewModel(creditCardViewModel);
		creditCardModel.setPaymentInformationViewModel(pp);
		ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		try {
			String query = ow.writeValueAsString(creditCardModel);

			//System.out.println(query);
			String encryptDataString = cm.encryptData(query);
			byte[]  encryptData  = Base64.getDecoder().decode(encryptDataString);
			String decryptData = cm.dycryptData(encryptData);
			System.out.println(decryptData);
			ObjectMapper mapper = new ObjectMapper();
			Map<String, Object> map;
			try {
				map = mapper.readValue(decryptData, Map.class);
				CreditCardModel model  = mapper.convertValue(map.get("creditCardViewModel"), CreditCardModel.class);
				System.out.println(model.getCreditCardNumber() + " " + model.getCardCode());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			//ObjectMapper mapper = new ObjectMapper(); // jackson's objectmapper
			//Map<String,Object> map = mapper.readValue(query, CreditCardModel.class);
		    //CreditCardModel crtCardModel = mapper.convertValue(query, CreditCardModel.class);
		   // System.out.println(crtCardModel);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		//cm.encryptData(creditCardInformation);
	}
}
