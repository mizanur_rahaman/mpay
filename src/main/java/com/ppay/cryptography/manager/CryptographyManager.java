package com.ppay.cryptography.manager;

import java.security.Key;
import java.util.Base64;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ppay.billing.manager.Billing;
import com.ppay.paymentservice.PaymentService;

@Service
public class CryptographyManager {
	private static final Logger logger = Logger.getLogger(CryptographyManager.class);
	@Autowired Billing billing;
	public byte[] encryptData(String data)
	{
		Key aesKey = new SecretKeySpec(billing.getAesKey().getBytes(), "AES");
		try 
		{
			Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
			cipher.init(Cipher.ENCRYPT_MODE, aesKey);
			byte[] encrypted = cipher.doFinal(data.getBytes());
			logger.info("Encrypted: " + new String(Base64.getUrlEncoder().encodeToString(encrypted)));
			return encrypted;
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}
		
		
		return null;
	}
	
	public String dycryptData(byte[] encrypted)
	{
		Key aesKey = new SecretKeySpec(billing.getAesKey().getBytes(), "AES");
		try {
			Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
			cipher.init(Cipher.DECRYPT_MODE, aesKey);
			String decrypted = new String(cipher.doFinal(encrypted));
			return decrypted;
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}
		return null;
	}
}
