﻿package com.ppay.dto;
public class PaymentInformationModel
{
    private String invoiceNumber;
    private double subTotal;
    private double discount;
    private double shippingAndHandling;
    private double salesTax;
    private double orderTotal;
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	public double getSubTotal() {
		return subTotal;
	}
	public void setSubTotal(double subTotal) {
		this.subTotal = subTotal;
	}
	public double getDiscount() {
		return discount;
	}
	public void setDiscount(double discount) {
		this.discount = discount;
	}
	public double getShippingAndHandling() {
		return shippingAndHandling;
	}
	public void setShippingAndHandling(double shippingAndHandling) {
		this.shippingAndHandling = shippingAndHandling;
	}
	public double getSalesTax() {
		return salesTax;
	}
	public void setSalesTax(double salesTax) {
		this.salesTax = salesTax;
	}
	public double getOrderTotal() {
		return orderTotal;
	}
	public void setOrderTotal(double orderTotal) {
		this.orderTotal = orderTotal;
	}
}

