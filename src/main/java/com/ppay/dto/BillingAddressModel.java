﻿package com.ppay.dto;
public class BillingAddressModel
{
    private String firstName;
    private String lastName;
    private String street;
    private String stree2;
    private String city;
    private String state;
    private String country;
    private String postalCode;
    private String emailAddress;
    private String phoneNumber;
    private String company;
    private String comment;
    private Boolean isBillingAndShippingAddressSame = false;
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getStree2() {
		return stree2;
	}
	public void setStree2(String stree2) {
		this.stree2 = stree2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public Boolean getIsBillingAndShippingAddressSame() {
		return isBillingAndShippingAddressSame;
	}
	public void setIsBillingAndShippingAddressSame(Boolean isBillingAndShippingAddressSame) {
		this.isBillingAndShippingAddressSame = isBillingAndShippingAddressSame;
	}

}

