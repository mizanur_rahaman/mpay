﻿package com.ppay.dto;

import org.apache.log4j.Logger;

import net.authorize.api.contract.v1.CreateTransactionResponse;
import net.authorize.api.contract.v1.TransactionResponse;

public class ANetTransactionResponseModel
{
	private static final Logger logger = Logger.getLogger(ANetTransactionResponseModel.class);

    private int responseCode;
    private String resultCode;
    private String avsResult;
    private String refId;
    private String cvvResult;
    private String transactionId;
    private String message;
    private String messageCode;
    private String accountNumber;
    private String accountType;
    private String errorMessage;


	public int getResponseCode() {
		return this.responseCode;
	}

	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}

	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	public String getAvsResult() {
		return avsResult;
	}

	public void setAvsResult(String avsResult) {
		this.avsResult = avsResult;
	}

	public String getRefId() {
		return refId;
	}

	public void setRefId(String refId) {
		this.refId = refId;
	}

	public String getCvvResult() {
		return cvvResult;
	}

	public void setCvvResult(String cvvResult) {
		this.cvvResult = cvvResult;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMessageCode() {
		return messageCode;
	}

	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
    public ANetTransactionResponseModel(CreateTransactionResponse response)
    {
    	try{
            if (response != null)
            {
                this.refId = response.getRefId();
                this.resultCode = response.getMessages() != null ?response.getMessages().getResultCode().toString() : "";
                logger.info(this.resultCode);
                if (response.getTransactionResponse() != null)
                {
                    responseCode = Integer.parseInt(response.getTransactionResponse().getResponseCode());
                    cvvResult = GetCvvResponseMessage(response.getTransactionResponse());
                    avsResult = GetAvsResponseMessage(response.getTransactionResponse());
                    transactionId = response.getTransactionResponse().getTransId();
                    accountNumber = response.getTransactionResponse().getAccountNumber();
                    accountType = response.getTransactionResponse().getAccountType();
                    errorMessage = response.getTransactionResponse().getErrors() != null ?response.getTransactionResponse().getErrors().toString() : "";
                    logger.error(errorMessage);
                }
                message = response.getMessages() != null ? (response.getMessages().getMessage().get(0) != null ? response.getMessages().getMessage().get(0).getText() : ""): "";
                logger.info(message);
                messageCode = response.getMessages() != null ? (response.getMessages().getMessage().get(0) != null ? response.getMessages().getMessage().get(0).getCode() : ""): "";
            }
    	}
    	catch(Exception ex)
    	{
    		logger.error(ex);
    	}

    }
    
    public ANetTransactionResponseModel() {
		// TODO Auto-generated constructor stub
	}

	private String GetAvsResponseMessage(TransactionResponse response)
    {
        switch (response.getAvsResultCode())
        {
            case "A":
                return "The street address matched, but the postal code did not.";
            case "B":
                return "No address information was provided.";
            case "E":
                return "The AVS check returned an error.";
            case "G":
                return "The card was issued by a bank outside the U.S. and does not support AVS.";
            case "N":
                return "Neither the street address nor postal code matched.";
            case "P":
                return "AVS is not applicable for this transaction.";
            case "R":
                return "Retry — AVS was unavailable or timed out.";
            case "S":
                return "AVS is not supported by card issuer.";
            case "U":
                return "Address information is unavailable.";
            case "W":
                return "The US ZIP+4 code matches, but the street address does not.";
            case "X":
                return "Both the street address and the US ZIP+4 code matched.";
            case "Y":
                return "The street address and postal code matched.";
            case "Z":
                return "The postal code matched, but the street address did not.";
            default:
                return "";
        }
    }
    private String GetCvvResponseMessage(TransactionResponse response)
    {
        switch (response.getCvvResultCode())
        {
            case "M":
                return "CVV matched";
            case "N":
                return "CVV did not match.";
            case "P":
                return "CVV was not processed.";
            case "S":
                return "CVV should have been present but was not indicated.";
            case "U":
                return "The issuer was unable to process the CVV check.";
            default:
                return "";
        }
    }
}
