﻿package com.ppay.dto;
public class ANetApiKeyModel
{
    private String apiLoginId ;
    private String apiTransactionKey ;
    private Boolean isOnProduction = false;
	public String getApiLoginId() {
		return apiLoginId;
	}
	public void setApiLoginId(String apiLoginId) {
		this.apiLoginId = apiLoginId;
	}
	public String getApiTransactionKey() {
		return apiTransactionKey;
	}
	public void setApiTransactionKey(String apiTransactionKey) {
		this.apiTransactionKey = apiTransactionKey;
	}
	public Boolean getIsOnProduction() {
		return isOnProduction;
	}
	public void setIsOnProduction(Boolean isOnProduction) {
		this.isOnProduction = isOnProduction;
	}

}

