package com.ppay.dto;


public class CreditCardPaymentModel {


	private ANetApiKeyModel aNetApiKeyViewModel = new ANetApiKeyModel();
	private CreditCardModel creditCardViewModel = new CreditCardModel();
	private PaymentInformationModel paymentInformationViewModel = new PaymentInformationModel();
	private BillingAddressModel billingAddressViewModel = new BillingAddressModel();
	private ShippingAddressModel shippingAddressViewModel = new ShippingAddressModel();
	public ANetApiKeyModel getaNetApiKeyViewModel() {
		return aNetApiKeyViewModel;
	}
	public void setaNetApiKeyViewModel(ANetApiKeyModel aNetApiKeyViewModel) {
		this.aNetApiKeyViewModel = aNetApiKeyViewModel;
	}
	public CreditCardModel getCreditCardViewModel() {
		return creditCardViewModel;
	}
	public void setCreditCardViewModel(CreditCardModel creditCardViewModel) {
		this.creditCardViewModel = creditCardViewModel;
	}
	public PaymentInformationModel getPaymentInformationViewModel() {
		return paymentInformationViewModel;
	}
	public void setPaymentInformationViewModel(PaymentInformationModel paymentInformationViewModel) {
		this.paymentInformationViewModel = paymentInformationViewModel;
	}
	public BillingAddressModel getBillingAddressViewModel() {
		return billingAddressViewModel;
	}
	public void setBillingAddressViewModel(BillingAddressModel billingAddressViewModel) {
		this.billingAddressViewModel = billingAddressViewModel;
	}
	public ShippingAddressModel getShippingAddressViewModel() {
		return shippingAddressViewModel;
	}
	public void setShippingAddressViewModel(ShippingAddressModel shippingAddressViewModel) {
		this.shippingAddressViewModel = shippingAddressViewModel;
	}
	

}
