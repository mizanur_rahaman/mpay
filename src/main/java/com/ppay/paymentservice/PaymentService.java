package com.ppay.paymentservice;

import java.io.IOException;
import java.util.Base64;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.ppay.billing.manager.ChargeCreditCard;
import com.ppay.controller.PaymentController;
import com.ppay.cryptography.manager.CryptographyManager;
import com.ppay.dto.ANetApiKeyModel;
import com.ppay.dto.ANetTransactionResponseModel;
import com.ppay.dto.ActionResult;
import com.ppay.dto.BillingAddressModel;
import com.ppay.dto.CreditCardDto;
import com.ppay.dto.CreditCardPaymentModel;
import com.ppay.dto.CreditCardModel;
import com.ppay.dto.PaymentInformationModel;
import com.ppay.dto.PaymentResult;
import com.ppay.dto.ShippingAddressModel;

@Service
public class PaymentService {
	private static final Logger logger = Logger.getLogger(PaymentService.class);

    @Autowired CryptographyManager cryptographyManager;
    @Autowired ChargeCreditCard chargeCreditCard;
	public ANetTransactionResponseModel processCreditCardPayment(String encCreditCardInformation) {
		try 
		{
			
			byte[] encryptedData = Base64.getUrlDecoder().decode(encCreditCardInformation);
			String query = cryptographyManager.dycryptData(encryptedData);
			logger.debug(query);
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
			Map<String, Object> clientInfo = mapper.readValue(query, Map.class);
			
			CreditCardPaymentModel creditCardPaymentModel = new CreditCardPaymentModel();
			creditCardPaymentModel.setaNetApiKeyViewModel(mapper.convertValue(clientInfo.get("ANetApiKeyViewModel"), ANetApiKeyModel.class));
			creditCardPaymentModel.setCreditCardViewModel(mapper.convertValue(clientInfo.get("CreditCardViewModel"), CreditCardModel.class));
			creditCardPaymentModel.setPaymentInformationViewModel(mapper.convertValue(clientInfo.get("PaymentInformationViewModel"), PaymentInformationModel.class));
			creditCardPaymentModel.setBillingAddressViewModel(mapper.convertValue(clientInfo.get("BillingAddressViewModel"), BillingAddressModel.class));
			creditCardPaymentModel.setShippingAddressViewModel(mapper.convertValue(clientInfo.get("ShippingAddressViewModel"), ShippingAddressModel.class));
			ANetTransactionResponseModel paymentResult = chargeCreditCard.creditCardCharge(creditCardPaymentModel.getaNetApiKeyViewModel(), creditCardPaymentModel.getCreditCardViewModel(), creditCardPaymentModel.getBillingAddressViewModel(), creditCardPaymentModel.getShippingAddressViewModel(), creditCardPaymentModel.getPaymentInformationViewModel());
			
			return paymentResult;

		} 
		catch (Exception e)
		{
			e.printStackTrace();
			logger.error(e);
		}
		
		return new ANetTransactionResponseModel();


	}

}
