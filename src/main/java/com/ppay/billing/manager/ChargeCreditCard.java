package com.ppay.billing.manager;

import java.math.BigDecimal;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.ppay.dto.ANetApiKeyModel;
import com.ppay.dto.ANetTransactionResponseModel;
import com.ppay.dto.BillingAddressModel;
import com.ppay.dto.CreditCardDto;
import com.ppay.dto.CreditCardModel;
import com.ppay.dto.PaymentInformationModel;
import com.ppay.dto.PaymentResult;
import com.ppay.dto.ShippingAddressModel;

import net.authorize.Environment;
import net.authorize.api.contract.v1.*;
import net.authorize.api.controller.base.ApiOperationBase;
import net.authorize.api.controller.CreateTransactionController;

@Service
public class ChargeCreditCard {
	private static final Logger logger = Logger.getLogger(ChargeCreditCard.class);

	public PaymentResult chargeCreditCard(CreditCardDto creditCardDto) {

    	PaymentResult paymentResult = new PaymentResult();
        //Common code to set for all requests
        ApiOperationBase.setEnvironment(Environment.SANDBOX);

        MerchantAuthenticationType merchantAuthenticationType  = new MerchantAuthenticationType() ;
        merchantAuthenticationType.setName(creditCardDto.getApiLoginId());
        merchantAuthenticationType.setTransactionKey(creditCardDto.getTransactionKey());
        ApiOperationBase.setMerchantAuthentication(merchantAuthenticationType);

        // Populate the payment data
        PaymentType paymentType = new PaymentType();
        CreditCardType creditCard = new CreditCardType();
        creditCard.setCardNumber(creditCardDto.getCardNumber());
        creditCard.setExpirationDate(creditCardDto.getExpirationDate());
        creditCard.setCardCode(creditCardDto.getCardCode());
        paymentType.setCreditCard(creditCard);

        // Create the payment transaction request
        TransactionRequestType txnRequest = new TransactionRequestType();
        txnRequest.setTransactionType(TransactionTypeEnum.AUTH_CAPTURE_TRANSACTION.value());
        txnRequest.setPayment(paymentType);
        txnRequest.setAmount(new BigDecimal(creditCardDto.getAmount()));
        
        // populate billing information
        CustomerAddressType customerAddressType = new CustomerAddressType();
        customerAddressType.setFirstName(creditCardDto.getFirstName());
        customerAddressType.setLastName(creditCardDto.getLastName());
        customerAddressType.setPhoneNumber(creditCardDto.getTelePhone());
        customerAddressType.setAddress(creditCardDto.getStreet());
        customerAddressType.setCity(creditCardDto.getCity());
        customerAddressType.setState(creditCardDto.getState());
        customerAddressType.setCountry(creditCardDto.getCountry());
        customerAddressType.setZip(creditCardDto.getPostalCode());
        customerAddressType.setCompany(creditCardDto.getCompany());
        customerAddressType.setEmail(creditCardDto.getEmailAddress());
        txnRequest.setBillTo(customerAddressType);

        // Make the API Request
        CreateTransactionRequest apiRequest = new CreateTransactionRequest();
        apiRequest.setTransactionRequest(txnRequest);
        apiRequest.setRefId(creditCardDto.getRefId());
        CreateTransactionController controller = new CreateTransactionController(apiRequest);
        controller.execute();


        CreateTransactionResponse response = controller.getApiResponse();
        System.out.println(response);
        if (response!=null) {

            // If API Response is ok, go ahead and check the transaction response
            if (response.getMessages().getResultCode() == MessageTypeEnum.OK) {

                TransactionResponse result = response.getTransactionResponse();
                if (result.getResponseCode().equals("1")) {
                    System.out.println(result.getResponseCode());
                    System.out.println("Successful Credit Card Transaction");
                    System.out.println(result.getAuthCode());
                    System.out.println(result.getTransId());
                    paymentResult.setResponseCode(result.getResponseCode());
                    paymentResult.setTransId(result.getTransId());
                    paymentResult.setAccountNumber(result.getAccountNumber());
                    paymentResult.setAccountType(result.getAccountType());
                    paymentResult.setRefId(response.getRefId());
                }
                else
                {
                    System.out.println("ok Failed Transaction"+result.getResponseCode());
                    paymentResult.setResponseCode(result.getResponseCode());
                    paymentResult.setRefId(response.getRefId());
                    paymentResult.setError(result.getErrors().getError());
                }
            }
            else
            {
                System.out.println("Failed Transaction:  "+response.getMessages().getResultCode());
                paymentResult.setResponseCode("3");
                paymentResult.setRefId(response.getRefId());
                paymentResult.setMessage(response.getMessages().getMessage());
            }
        }
        return paymentResult;
    }
	
	public ANetTransactionResponseModel creditCardCharge(ANetApiKeyModel apiViewModel,CreditCardModel creditCardViewModel,BillingAddressModel billingAddressViewModel,ShippingAddressModel shippingAddressViewModel,PaymentInformationModel paymentInformationViewModel)
    {
		try
		{
			logger.info("billing info: name: "+ billingAddressViewModel.getFirstName() + " " + billingAddressViewModel.getLastName());
			logger.info("billing info: email: "+ billingAddressViewModel.getEmailAddress() + " , phone: " + billingAddressViewModel.getPhoneNumber());
	        //Common code to set for all requests
	        ApiOperationBase.setEnvironment(apiViewModel.getIsOnProduction() != null && apiViewModel.getIsOnProduction() == true ? Environment.PRODUCTION : Environment.SANDBOX);
	
	        ApiOperationBase.setMerchantAuthentication(setMerchantAuthentication(apiViewModel));
	
	        // Populate the payment data
	        PaymentType paymentType = new PaymentType();
	        paymentType.setCreditCard(setCreditCardInfo(creditCardViewModel));
	
	        // Create the payment transaction request
	        TransactionRequestType txnRequest = new TransactionRequestType();
	        txnRequest.setTransactionType(TransactionTypeEnum.AUTH_CAPTURE_TRANSACTION.value());
	        txnRequest.setPayment(paymentType);
	        
	        logger.info("order total: "+paymentInformationViewModel.getOrderTotal() + ", shipping: " + paymentInformationViewModel.getShippingAndHandling() + ", tax: " + paymentInformationViewModel.getSalesTax());
	        
	        BigDecimal orderTotal = new BigDecimal(paymentInformationViewModel.getOrderTotal());
	        orderTotal = orderTotal.setScale(2, BigDecimal.ROUND_HALF_EVEN);
	        txnRequest.setAmount(orderTotal);
	        
		    ExtendedAmountType tax = new ExtendedAmountType();
		    BigDecimal taxAmount = new BigDecimal(paymentInformationViewModel.getSalesTax());
		    taxAmount = taxAmount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
		    tax.setAmount(taxAmount);
		    txnRequest.setTax(tax);
		    
		    ExtendedAmountType shipping = new ExtendedAmountType();
		    BigDecimal shippingAmount = new BigDecimal(paymentInformationViewModel.getShippingAndHandling());
		    shippingAmount = shippingAmount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
		    shipping.setAmount(shippingAmount);
		    txnRequest.setShipping(shipping);
		    
		    OrderType ordertype = new OrderType();
		    ordertype.setInvoiceNumber(paymentInformationViewModel.getInvoiceNumber());
		    ordertype.setDescription(billingAddressViewModel.getComment());
		    txnRequest.setOrder(ordertype);
	        
	        // populate billing information
	        txnRequest.setBillTo(setBillingAddress(billingAddressViewModel));
	        
	        // populate Shipping Information
	        txnRequest.setShipTo(setShippingAddress(billingAddressViewModel,shippingAddressViewModel));
	
	        // Make the API Request
	        CreateTransactionRequest apiRequest = new CreateTransactionRequest();
	        apiRequest.setTransactionRequest(txnRequest);
	        //apiRequest.setRefId(creditCardDto.getRefId());
	        CreateTransactionController controller = new CreateTransactionController(apiRequest);
	        controller.execute();
	
	
	        CreateTransactionResponse response = controller.getApiResponse();
	        logger.debug(response);

	        return new ANetTransactionResponseModel(response);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.error(ex);
		}
        return new ANetTransactionResponseModel();

    }
	
	public MerchantAuthenticationType setMerchantAuthentication(ANetApiKeyModel apiViewModel){
		MerchantAuthenticationType merchantAuthenticationType  = new MerchantAuthenticationType() ;
        merchantAuthenticationType.setName(apiViewModel.getApiLoginId());
        merchantAuthenticationType.setTransactionKey(apiViewModel.getApiTransactionKey());
        return merchantAuthenticationType;
	}
	
	public CreditCardType setCreditCardInfo(CreditCardModel creditCardViewModel)
	{
        CreditCardType creditCard = new CreditCardType();
        creditCard.setCardNumber(creditCardViewModel.getCreditCardNumber());
        creditCard.setExpirationDate(creditCardViewModel.getExpirationDate());
        creditCard.setCardCode(creditCardViewModel.getCardCode());
        
        return creditCard;
	}
	
	public CustomerAddressType setBillingAddress(BillingAddressModel billingAddressViewModel)
	{
        CustomerAddressType customerAddressType = new CustomerAddressType();
        customerAddressType.setFirstName(billingAddressViewModel.getFirstName());
        customerAddressType.setLastName(billingAddressViewModel.getLastName());
        customerAddressType.setPhoneNumber(billingAddressViewModel.getPhoneNumber());
        customerAddressType.setAddress(billingAddressViewModel.getStreet());
        customerAddressType.setCity(billingAddressViewModel.getCity());
        customerAddressType.setState(billingAddressViewModel.getState());
        customerAddressType.setCountry(billingAddressViewModel.getCountry());
        customerAddressType.setZip(billingAddressViewModel.getPostalCode());
        customerAddressType.setCompany(billingAddressViewModel.getCompany());
        customerAddressType.setEmail(billingAddressViewModel.getEmailAddress());
        return customerAddressType;
	}
	
	public CustomerAddressType setShippingAddress(BillingAddressModel billingAddressViewModel,ShippingAddressModel shippingAddressViewModel)
	{
        CustomerAddressType shippingAddressType = new CustomerAddressType();

        if(billingAddressViewModel.getIsBillingAndShippingAddressSame() != null && billingAddressViewModel.getIsBillingAndShippingAddressSame() == false)
        {
	        shippingAddressType.setFirstName(shippingAddressViewModel.getFirstName());
	        shippingAddressType.setLastName(shippingAddressViewModel.getLastName());
	        shippingAddressType.setAddress( shippingAddressViewModel.getStreet());
	        shippingAddressType.setCity(shippingAddressViewModel.getCity());
	        shippingAddressType.setState(shippingAddressViewModel.getState());
	        shippingAddressType.setCountry( shippingAddressViewModel.getCountry());
	        shippingAddressType.setZip(shippingAddressViewModel.getPostalCode());
        }
        else
        {
	        shippingAddressType.setFirstName(billingAddressViewModel.getFirstName());
	        shippingAddressType.setLastName(billingAddressViewModel.getLastName());
	        shippingAddressType.setAddress(billingAddressViewModel.getStreet());
	        shippingAddressType.setCity(billingAddressViewModel.getCity());
	        shippingAddressType.setState(billingAddressViewModel.getState());
	        shippingAddressType.setCountry(billingAddressViewModel.getCountry());
	        shippingAddressType.setZip(billingAddressViewModel.getPostalCode());
        }
        return shippingAddressType;
	}
}
