package com.ppay.billing.manager;

import org.springframework.context.annotation.Configuration;

@Configuration
public class Billing {
	private String environment;
	private String apiLoginId;
	private String transactionKey;
	private String secretKey;
	private String aesKey;
	public String getEnvironment() {
		return environment;
	}
	public void setEnvironment(String environment) {
		this.environment = environment;
	}
	public String getApiLoginId() {
		return apiLoginId;
	}
	public void setApiLoginId(String apiLoginId) {
		this.apiLoginId = apiLoginId;
	}
	public String getTransactionKey() {
		return transactionKey;
	}
	public void setTransactionKey(String transactionKey) {
		this.transactionKey = transactionKey;
	}
	public String getSecretKey() {
		return secretKey;
	}
	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}
	public String getAesKey() {
		return aesKey;
	}
	public void setAesKey(String aesKey) {
		this.aesKey = aesKey;
	}
}
